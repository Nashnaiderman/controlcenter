package com.example.controlcenter

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented printCurrencyTests, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under printCurrencyTests.
        val appContext = ApplicationProvider.getApplicationContext<Application>()
        assertEquals("com.example.controlcenter", appContext.packageName)
    }
}
