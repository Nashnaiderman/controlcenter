package com.example.controlcenter;

import android.util.Log;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

public class NumericTests {
    private static final String TAG = NumericTests.class.getSimpleName();

    public static String printCurrencyTests(float value) {
        String result;
        Locale locale = new Locale("pt", "BR");

        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setRoundingMode(RoundingMode.DOWN);

        result = numberFormat.format(value);
        Log.d(TAG, "Result 1: " + result);

        result = numberFormat.format((double) value);
        Log.d(TAG, "Result 2: " + result);

        String tmpFloat = Float.valueOf(value).toString();
        Double tmpDouble = Double.valueOf(tmpFloat);
        result = numberFormat.format(tmpDouble);
        Log.d(TAG, "Result 3: " + result);

        double tmpDouble2 = Float.valueOf(value).doubleValue();
        result = numberFormat.format(tmpDouble2);
        Log.d(TAG, "Result 4: " + result);

        result = String.format(locale,"R$ %.2f", value);
        Log.d(TAG, "Result 5: " + result);

        return result;
    }
}
