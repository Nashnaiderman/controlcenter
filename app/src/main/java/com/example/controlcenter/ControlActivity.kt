package com.example.controlcenter

import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class ControlActivity : AppCompatActivity() {
    private val TAG = ControlActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_control)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)

        if (hasFocus) {
            hideSystemUI()
        }
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                Log.d(TAG, "x: ${event.x} \t y: ${event.y}")
            }
            MotionEvent.ACTION_MOVE -> {
//                Log.d(TAG, "x: ${event.x} \t y: ${event.y}")
            }
            MotionEvent.ACTION_UP -> {
//                Log.d(TAG, "x: ${event.x} \t y: ${event.y}")
            }
            MotionEvent.ACTION_CANCEL -> {

            }
            else -> {

            }
        }
        return super.onTouchEvent(event)
    }
}
