package com.example.controlcenter

import android.animation.*
import android.content.Context
import android.graphics.BlurMaskFilter
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import kotlin.math.sin

class WifiLayout : RelativeLayout {
    private val TAG = WifiLayout::class.java.simpleName
    private val START_DELAY = 200L
    private val ANIMATION_DURATION = 300L
    private val HALF_ANIMATION_DURATION = ANIMATION_DURATION / 2

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    private var wifiColor = 0
    private var wifiCenterRadius = 0f
    private var wifiBaseRadius = 0f
    private var wifiSweepAngle = 0f
    private var wifiArcOffset = 0f
    private var wifiArcCount = 0
    private val animatorSet = AnimatorSet().apply {
        interpolator = SinInterpolator()
    }

    private fun init(attrs: AttributeSet?) {
        if (isInEditMode) return

        requireNotNull(attrs) { "Missing attributes of the view" }

        context.obtainStyledAttributes(attrs, R.styleable.WifiLayout).apply {
            wifiColor = getColor(R.styleable.WifiLayout_wifi_color, ContextCompat.getColor(context, R.color.colorAccent))
            wifiCenterRadius = getFloat(R.styleable.WifiLayout_wifi_center_radius, 24f)
            wifiBaseRadius = getFloat(R.styleable.WifiLayout_wifi_base_radius, 60f)
            wifiSweepAngle = getFloat(R.styleable.WifiLayout_wifi_sweep_angle, 100f)
            wifiArcOffset = getFloat(R.styleable.WifiLayout_wifi_arc_offset, 45f)
            wifiArcCount = getInteger(R.styleable.WifiLayout_wifi_arc_count, 3)
            recycle()
        }

        val animatorList = ArrayList<Animator>()

        WifiCenter().let {
            addView(it)

            animatorList.add(
                ObjectAnimator.ofPropertyValuesHolder(
                    it,
                    PropertyValuesHolder.ofFloat("ScaleX", 1f, 1.8f),
                    PropertyValuesHolder.ofFloat("ScaleY", 1f, 1.8f)
                ).apply {
                    duration = ANIMATION_DURATION
                    startDelay = 0
                }
            )
        }

        var radius = wifiBaseRadius
        var scale = 1.2f

        for (i in 1..wifiArcCount) {
            WifiArc(radius).let {
                addView(it)

                animatorList.add(
                    ObjectAnimator.ofPropertyValuesHolder(
                        it,
                        PropertyValuesHolder.ofFloat("ScaleX", 1f, scale),
                        PropertyValuesHolder.ofFloat("ScaleY", 1f, scale)
                    ).apply {
                        duration = ANIMATION_DURATION
                        startDelay = i * HALF_ANIMATION_DURATION
                    }
                )
            }
            radius += wifiArcOffset
            scale -= 0.04f
        }

        animatorSet.apply {
            playTogether(animatorList)
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {}
                override fun onAnimationCancel(animation: Animator?) {}
                override fun onAnimationStart(animation: Animator?) {}

                override fun onAnimationEnd(animation: Animator?) {
                    animatorSet.apply {
                        startDelay = START_DELAY
                        start()
                    }
                }

            })
        }
    }

    fun startPauseAnimation() {
        animatorSet.run {
            when {
                isPaused -> resume()
                isStarted -> pause()
                else -> {
                    startDelay = START_DELAY
                    start()
                }
            }
        }
    }

    inner class WifiArc(private val radius: Float) : View(context) {
        private val paint = Paint().apply {
            style = Paint.Style.STROKE
            strokeWidth = 32f
            strokeCap = Paint.Cap.BUTT
            isAntiAlias = true
            color = wifiColor
            maskFilter = BlurMaskFilter(20f, BlurMaskFilter.Blur.INNER)
        }
        private val rect = RectF()

        override fun onDraw(canvas: Canvas?) {
            super.onDraw(canvas)

            val centerX = width / 2f
            val centerY = height / 2f

            rect.left = centerX - radius
            rect.top = centerY - radius
            rect.right = centerX + radius
            rect.bottom = centerY + radius

            val startingAngle = -(wifiSweepAngle / 2f)

            canvas?.apply {
                drawArc(rect, startingAngle, wifiSweepAngle, false, paint)
                drawArc(rect, 180 + startingAngle, wifiSweepAngle, false, paint)
            }
        }
    }

    inner class WifiCenter : View(context) {
        private val paint = Paint().apply {
            style = Paint.Style.FILL
            isAntiAlias = true
            color = wifiColor
            maskFilter = BlurMaskFilter(20f, BlurMaskFilter.Blur.INNER)
        }

        override fun onDraw(canvas: Canvas?) {
            super.onDraw(canvas)

            canvas?.drawCircle(width / 2f, height / 2f, wifiCenterRadius, paint)
        }
    }

    class SinInterpolator : TimeInterpolator {
        override fun getInterpolation(t: Float) = sin(t * Math.PI).toFloat()
    }
}