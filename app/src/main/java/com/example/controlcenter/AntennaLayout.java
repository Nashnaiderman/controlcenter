package com.example.controlcenter;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import androidx.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class AntennaLayout extends RelativeLayout {
    private static final String TAG = AntennaLayout.class.getSimpleName();

    private static final float DEFAULT_WAVE_RADIUS = 32f;
    private static final float DEFAULT_WAVE_SCALE = 6f;
    private static final int DEFAULT_WAVE_COUNT = 5;

    private static final float DEFAULT_ANTENNA_CENTER_RADIUS = 24f;
    private static final float DEFAULT_ANTENNA_BASE_RADIUS = 60f;
    private static final float DEFAULT_ANTENNA_SWEEP_ANGLE = 100f;
    private static final float DEFAULT_ANTENNA_ARC_OFFSET = 45f;
    private static final int DEFAULT_ANTENNA_ARC_COUNT = 3;

    private static final long ANIMATION_DURATION = 300;
    private static final long HALF_ANIMATION_DURATION = ANIMATION_DURATION / 2;
    private static final long ANIMATION_START_DELAY = 200;
    private static final long WAVE_DURATION = 200;

    private static final int CENTER_BLUR_RADIUS = 20;
    private static final int ARC_BLUR_RADIUS = 20;

    private @ColorInt int waveColor;
    private float waveRadius;
    private float waveScale;
    private int waveCount;

    private @ColorInt int antennaColor;
    private float antennaCenterRadius;
    private float antennaBaseRadius;
    private float antennaSweepAngle;
    private float antennaArcOffset;
    private int antennaArcCount;

    private ArrayList<WaveView> waveViewList = new ArrayList<>();
    private AnimatorSet antennaAnimatorSet;
    private AnimatorSet waveAnimatorSet;

    public AntennaLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AntennaLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(final Context context, final AttributeSet attrs) {
        if (isInEditMode()) {
            return;
        }

        if (null == attrs) {
            throw new IllegalArgumentException("Missing attributes of this view");
        }

//        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AntennaView);

//        waveColor = typedArray.getColor(R.styleable.AntennaView_wave_color, ContextCompat.getColor(context, R.color.colorAccent));
//        waveRadius = typedArray.getFloat(R.styleable.AntennaView_wave_radius, DEFAULT_WAVE_RADIUS);
//        waveCount = typedArray.getInt(R.styleable.AntennaView_wave_count, DEFAULT_WAVE_COUNT);
//        waveScale = typedArray.getFloat(R.styleable.AntennaView_wave_scale, DEFAULT_WAVE_SCALE);
//
//        antennaColor = typedArray.getColor(R.styleable.AntennaView_antenna_color, ContextCompat.getColor(context, R.color.colorAccent));
//        antennaCenterRadius = typedArray.getFloat(R.styleable.AntennaView_antenna_center_radius, DEFAULT_ANTENNA_CENTER_RADIUS);
//        antennaBaseRadius = typedArray.getFloat(R.styleable.AntennaView_antenna_base_radius, DEFAULT_ANTENNA_BASE_RADIUS);
//        antennaSweepAngle = typedArray.getFloat(R.styleable.AntennaView_antenna_sweep_angle, DEFAULT_ANTENNA_SWEEP_ANGLE);
//        antennaArcOffset = typedArray.getFloat(R.styleable.AntennaView_antenna_arc_offset, DEFAULT_ANTENNA_ARC_OFFSET);
//        antennaArcCount = typedArray.getInt(R.styleable.AntennaView_antenna_arc_count, DEFAULT_ANTENNA_ARC_COUNT);
//        typedArray.recycle();

        setupAnimations(context);
    }

    private void setupAnimations(final Context context) {
        antennaAnimatorSet = new AnimatorSet();
        waveAnimatorSet = new AnimatorSet();
        ObjectAnimator scaleXAnimation;
        ObjectAnimator scaleYAnimation;

        antennaAnimatorSet.setInterpolator(new SinInterpolator());

        SignalCenter signalCenter = new SignalCenter(context, antennaCenterRadius);
        addView(signalCenter);

        scaleXAnimation = ObjectAnimator.ofFloat(signalCenter, "ScaleX", 1f, 1.7f);
        scaleXAnimation.setStartDelay(0);
        scaleXAnimation.setDuration(ANIMATION_DURATION);

        scaleYAnimation = ObjectAnimator.ofFloat(signalCenter, "ScaleY", 1f, 1.7f);
        scaleYAnimation.setStartDelay(0);
        scaleYAnimation.setDuration(ANIMATION_DURATION);

        antennaAnimatorSet.play(scaleXAnimation).with(scaleYAnimation);

        int i;
        WaveView waveView;
        ObjectAnimator alphaAnimation;
        // TODO adjust value
        long waveDelay = ((antennaArcCount * ANIMATION_DURATION) + ANIMATION_START_DELAY);
        long waveDuration = waveDelay * 2;
//        long waveDuration = (((antennaArcCount + 1) * ANIMATION_DURATION) + ANIMATION_START_DELAY);

        for (i = 0; i < 1; ++i) {
            waveView = new WaveView(context, waveRadius);
            addView(waveView);
            waveViewList.add(waveView);

            scaleXAnimation = ObjectAnimator.ofFloat(waveView, "ScaleX", 1, waveScale);
//            scaleXAnimation.setRepeatCount(ObjectAnimator.INFINITE);
//            scaleXAnimation.setRepeatMode(ObjectAnimator.RESTART);
            scaleXAnimation.setStartDelay((i * waveDelay) + HALF_ANIMATION_DURATION);
            scaleXAnimation.setDuration(waveDuration);

            scaleYAnimation = ObjectAnimator.ofFloat(waveView, "ScaleY", 1, waveScale);
//            scaleYAnimation.setRepeatCount(ObjectAnimator.INFINITE);
//            scaleYAnimation.setRepeatMode(ObjectAnimator.RESTART);
            scaleYAnimation.setStartDelay(scaleXAnimation.getStartDelay());
            scaleYAnimation.setDuration(waveDuration);

            alphaAnimation = ObjectAnimator.ofFloat(waveView, "Alpha", 1.0f, 0f);
//            alphaAnimation.setRepeatCount(ObjectAnimator.INFINITE);
//            alphaAnimation.setRepeatMode(ObjectAnimator.RESTART);
            alphaAnimation.setStartDelay(scaleXAnimation.getStartDelay());
            alphaAnimation.setDuration(waveDuration);

            waveAnimatorSet.play(scaleXAnimation).with(scaleYAnimation).with(alphaAnimation);
        }
        waveAnimatorSet.addListener(setupWaveAnimation());

        float radius = antennaBaseRadius;
        SignalArc signalArc;
        float scale = 1.2f;

        for (i = 1; i <= antennaArcCount; ++i) {
            signalArc = new SignalArc(context, radius);
            addView(signalArc);

            scaleXAnimation = ObjectAnimator.ofFloat(signalArc, "ScaleX", 1f, scale);
            scaleXAnimation.setStartDelay(i * HALF_ANIMATION_DURATION);
            scaleXAnimation.setDuration(ANIMATION_DURATION);

            scaleYAnimation = ObjectAnimator.ofFloat(signalArc, "ScaleY", 1f, scale);
            scaleYAnimation.setStartDelay(scaleXAnimation.getStartDelay());
            scaleYAnimation.setDuration(ANIMATION_DURATION);

            antennaAnimatorSet.play(scaleXAnimation).with(scaleYAnimation);

            radius += antennaArcOffset;
            scale -= 0.04f;
        }
        antennaAnimatorSet.addListener(setupAnimationLoop());
    }

    private Animator.AnimatorListener setupWaveAnimation() {
        return new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                for (WaveView waveView : waveViewList) {
                    waveView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                for (WaveView waveView : waveViewList) {
                    waveView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        };
    }

    private Animator.AnimatorListener setupAnimationLoop() {
        return new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                antennaAnimatorSet.setStartDelay(ANIMATION_START_DELAY);
                antennaAnimatorSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        };
    }

    public void startAnimation() {
        if (!antennaAnimatorSet.isRunning()) {
            antennaAnimatorSet.setStartDelay(ANIMATION_START_DELAY);
            waveAnimatorSet.setStartDelay(ANIMATION_START_DELAY);
            antennaAnimatorSet.start();
            waveAnimatorSet.start();
        }
    }

    public void stopAnimation() {
        if (antennaAnimatorSet.isRunning()) {
            antennaAnimatorSet.end();
            waveAnimatorSet.end();
        }
    }

    private class SignalCenter extends View {
        private Paint paint;
        private float radius;

        public SignalCenter(Context context, float radius) {
            super(context);

            this.radius = radius;

            paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setAntiAlias(true);
            paint.setColor(antennaColor);
            paint.setMaskFilter(new BlurMaskFilter(CENTER_BLUR_RADIUS, BlurMaskFilter.Blur.INNER));
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            float centerX = getWidth() / 2f;
            float centerY = getHeight() / 2f;

            canvas.drawCircle(centerX, centerY, radius, paint);
        }
    }

    private class SignalArc extends View {
        private Paint paint;
        private RectF rect;
        private float radius;

        public SignalArc(Context context, float radius) {
            super(context);

            this.radius = radius;

            paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            // TODO customize stroke width
            paint.setStrokeWidth(26f);
            paint.setStrokeCap(Paint.Cap.BUTT);
            paint.setAntiAlias(true);
            paint.setColor(antennaColor);
            paint.setMaskFilter(new BlurMaskFilter(ARC_BLUR_RADIUS, BlurMaskFilter.Blur.INNER));

            rect = new RectF();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            float centerX = getWidth() / 2f;
            float centerY = getHeight() / 2f;

            rect.left = centerX - radius;
            rect.top = centerY - radius;
            rect.right = centerX + radius;
            rect.bottom = centerY + radius;

            float startingAngle = -(antennaSweepAngle / 2f);

            canvas.drawArc(rect, startingAngle, antennaSweepAngle, false, paint);
            canvas.drawArc(rect, 180 + startingAngle, antennaSweepAngle, false, paint);
        }
    }

    private class WaveView extends View {
        private Paint paint;
        private float radius;

        public WaveView(Context context, float radius) {
            super(context);

            this.radius = radius;

            paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setAntiAlias(true);
            paint.setColor(waveColor);
            paint.setMaskFilter(new BlurMaskFilter(radius, BlurMaskFilter.Blur.INNER));

            this.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            float centerX = getWidth() / 2f;
            float centerY = getHeight() / 2f;

            canvas.drawCircle(centerX, centerY, radius, paint);
        }
    }

    private class SinInterpolator implements TimeInterpolator {
        @Override
        public float getInterpolation(float t) {
            return (float) Math.sin(t * Math.PI);
        }
    }
}
