package com.example.controlcenter

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.graphics.BlurMaskFilter
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat

class RippleLayout : RelativeLayout {
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    private var rippleColor = 0
    private var rippleRadius = 0f
    private var rippleStartDelay = 0L
    private var rippleDelay = 0L
    private var rippleDuration = 0L
    private var rippleScale = 0f
    private val animatorSet = AnimatorSet()

    private fun init(attrs: AttributeSet?) {
        if (isInEditMode) return

        requireNotNull(attrs) { "Missing attributes of the view" }

        context.obtainStyledAttributes(attrs, R.styleable.RippleLayout).apply {
            rippleColor = getColor(R.styleable.RippleLayout_ripple_color, ContextCompat.getColor(context, R.color.colorAccent))
            rippleRadius = getFloat(R.styleable.RippleLayout_ripple_radius, 32f)
            rippleStartDelay = getInt(R.styleable.RippleLayout_ripple_start_delay, 0).toLong()
            rippleDelay = getInt(R.styleable.RippleLayout_ripple_delay, 1000).toLong()
            rippleDuration = getInt(R.styleable.RippleLayout_ripple_duration, 3000).toLong()
            rippleScale = getFloat(R.styleable.RippleLayout_ripple_scale, 6f)
            recycle()
        }

        val layoutParams = LayoutParams((2 * rippleRadius).toInt(), (2 * rippleRadius.toInt())).apply {
            addRule(CENTER_IN_PARENT, TRUE)
        }
        val animatorList = ArrayList<Animator>()
        val rippleCount = (rippleDuration / rippleDelay).toInt()
        for (i in 0 until rippleCount) {
            RippleView().let {
                addView(it, layoutParams)
                animatorList.add(
                    ObjectAnimator.ofPropertyValuesHolder(
                        it,
                        PropertyValuesHolder.ofFloat("ScaleX", 1f, rippleScale),
                        PropertyValuesHolder.ofFloat("ScaleY", 1f, rippleScale)
                    ).apply {
                        repeatCount = ObjectAnimator.INFINITE
                        repeatMode = ObjectAnimator.RESTART
                        startDelay = rippleDelay
                        duration = rippleDuration
                    }
                )
                animatorList.add(ObjectAnimator.ofFloat(it, "Alpha", 1f, 0f).apply {
                    repeatCount = ObjectAnimator.INFINITE
                    repeatMode = ObjectAnimator.RESTART
                    startDelay = rippleDelay
                    duration = rippleDuration
                })
                rippleDelay += rippleDelay
            }
        }
        animatorSet.apply {
            playTogether(animatorList)
        }
    }

    fun startPauseAnimation() {
        animatorSet.run {
            when {
                isPaused -> resume()
                isStarted -> pause()
                else -> {
//                    startDelay = 350L
                    start()
                }
            }
        }
    }

    inner class RippleView : View(context) {
        private val paint = Paint().apply {
            style = Paint.Style.FILL
            isAntiAlias = true
            color = rippleColor
            maskFilter = BlurMaskFilter(rippleRadius, BlurMaskFilter.Blur.INNER)
        }

//        init {
//            visibility = INVISIBLE
//        }

        override fun onDraw(canvas: Canvas?) {
            super.onDraw(canvas)

            canvas?.drawCircle(width / 2f, height / 2f, rippleRadius, paint)
        }
    }
}